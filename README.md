# README #
### Required Tools ###
 * Composer
 * php 7.1 (php71)
 * php-dbg 7.1 (php71-php-dbg)
### Getting started ###
after cloning run:
`composer install`
### Basics ###
**Spec a file:**
`vendor/bin/phpspec desc [namespace]`
or `composer run-script spec [namespace]`

e.g. `composer run-script spec Example/Example` will create a file spec/Example/EampleSpec.php allowing you to add tests.

**Run phpspec tests:**
`vendor/bin/phpspec run` or

`composer run-script spec [namespace]` or

`php71-phpdbg -qrr vendor/phpspec/phpspec/bin/phpspec run`

to add code coverage add `--config=phpspecCoverage.yml`

**PSR-2 format checking:**
`vendor/bin/phpcs  --standard=ruleset-ci.xml [optional:file]` or

`vendor/bin/phpcs  --standard=ruleset.xml [optional:file]` (strict)


